# ics-ans-role-opendistro-kibana

Ansible role to install opendistro-kibana.

## Role Variables

```yaml
elasticsearch_hosts: https://localhost:9200

kibana_template:
  - name: config
    file: kibana.yml.j2
    dest: /etc/kibana/kibana.yml

...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-opendistro-kibana
```

## License

BSD 2-clause
